**Welcome to the AI Workshop tutorial!**

We will be going through a couple of Jupyter Notebooks during the tutorial and use a number of data science libraries along the way. The easiest way to get started is to download Anaconda, which is free and open source. When you download this, it comes with the Jupyter Notebook IDE and many popular data science libraries, so you don’t have to install them one by one.

Here are the steps you’ll need to take before the start of the tutorial:

1. Download Anaconda [here](https://www.anaconda.com/products/individual)
2. Download the Jupyter Notebooks

Clone or download this Github repository, so you have access to all the Jupyter Notebooks (.ipynb extension) in the tutorial. Note the green button on the right side of the screen that says Clone or download. If you know how to use Github, go ahead and clone the repo. If you don't know how to use Github, you can also just download the zip file and unzip it on your laptop.

3. Launch Anaconda and Open Jupyter Notebook

Open the Anaconda Navigator program. You should see the Jupyter Notebook logo. Below the logo, click Launch. A browser window should open up. In the browser window, navigate to the location of the saved Jupyter Notebook files and open Data_Processing.ipynb. Follow the instructions in the notebook.
